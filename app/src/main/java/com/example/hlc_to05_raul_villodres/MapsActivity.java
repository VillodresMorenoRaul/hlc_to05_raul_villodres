package com.example.hlc_to05_raul_villodres;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback  {

    private GoogleMap mMap;
    Polyline polyline = null;
    List<LatLng> latLngList = new ArrayList<>();
    Double area;
    String areaAString;
    DecimalFormat f = new DecimalFormat("##.00");
    Double perimetro;
    String perimetroAString;
    Boolean puntoInicialListo = false;
    Boolean cerrado = false;
    Marker marcador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //Preparamos el mapa
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Añadimos las coordenadas de Málaga y la animamos para que acceda directamente a la localización adecuada
        double latitud = 36.71961083514688;
        double longitud =  -4.453055248124418;
        LatLng malaga = new LatLng(latitud, longitud);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(malaga, 15), 7000, null);

        //Ponemos el onLongClickListener, la acción cuando hagamos un toque mantenido en el móvil
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                if(puntoInicialListo == false){
                    latLngList.add(latLng);

                    if(polyline != null) {
                        polyline.remove();
                    }

                    PolylineOptions polylineOptions = new PolylineOptions().addAll(latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);

                    mostrarMensaje("Coordenadas: " + latLng + "Añadidas correctamente");
                    marcador = mMap.addMarker(new MarkerOptions().position(latLng).title("Origen"));

                    puntoInicialListo = true;
                }

            }
        });

        //Creamos el OnMapClickListener
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if(puntoInicialListo == true && cerrado == false){
                    latLngList.add(latLng);
                    if(polyline != null) {
                        polyline.remove();
                    }

                    PolylineOptions polylineOptions = new PolylineOptions().addAll(latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                } else {

                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Area:
                computeArea(latLngList);
                break;

            case R.id.Perimetro:
                computeLength(latLngList);
                break;

            case R.id.Borrar:
                metodoBorrar();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void computeArea(List<LatLng> latLngList){

        if(latLngList.size() >= 3){

            //Si no está cerrado se cierra
            if(!cerrado){
                this.latLngList.add(this.latLngList.get(0));
                if(polyline!=null){
                    polyline.remove();

                    //Si falla sacar del condicional estas lineas
                    PolylineOptions polylineOptions = new PolylineOptions().addAll(this.latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                    cerrado = true;
                }
            }

            area = SphericalUtil.computeArea(latLngList);
            areaAString = f.format(area);
            mostrarMensaje("Area: " + areaAString + " metros cuadrados");
        } else {

            mostrarMensaje("Hacen falta al menos tres puntos de coordenadas para calcular el area");

        }

    }

    public void computeLength(List<LatLng> latLngList){

        if(latLngList.size() >= 3){
            //Cerramos si no lo está ya
            if(!cerrado) {
                this.latLngList.add(this.latLngList.get(0));
                if (polyline != null) {
                    polyline.remove();

                    //Si falla sacar del condicional estas lineas
                    PolylineOptions polylineOptions = new PolylineOptions().addAll(this.latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                }
            }

            perimetro = SphericalUtil.computeLength(latLngList);
            perimetroAString = f.format(perimetro);
            mostrarMensaje("Perimetro: " +perimetroAString + " Metros");
            cerrado = true;

        } else {
            mostrarMensaje("Hacen falta al menos dos puntos de coordenadas para calcular su perimetro");
        }
    }

    //El método borrar sólo actuará si tenemos un polygono
    public void metodoBorrar(){
        if(polyline != null && latLngList.size() >= 1 ){
            polyline.remove();
            latLngList.clear();
            marcador.remove();
            puntoInicialListo = false;
            cerrado = false;
        } else {
            mostrarMensaje("Hace falta al menos una coordenada para poder borrar");
        }
    }

    public void mostrarMensaje(String texto){
        Toast.makeText(MapsActivity.this, texto ,Toast.LENGTH_SHORT).show();
    }

}