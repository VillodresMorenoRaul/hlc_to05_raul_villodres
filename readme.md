# HLC_TO05 # 
Esta tarea consiste en realizar una actividad de android studio que involucra un mapa que hace uso de la API de google maps. La tarea involucra pues dos partes que se hacen fuera de android studio, y que son esenciales para que funcione correctamente.

## Preparando la Api Key desde google ## 

### Activamos Billing ### 
Simplemente entramos en [https://console.cloud.google.com/billing/](https://console.cloud.google.com/billing/ "https://console.cloud.google.com/billing/") y ponemos nuestra tarjeta de crédito o débito con la que pagaremos en caso de superar la cuota en "forma de pago". Y es necesario incorporarlo para poder crear una clave API válida
### Creando un proyecto ### 
Para preparar nuestra API, debemos de acceder a [https://console.cloud.google.com/projectselector2/home/](https://console.cloud.google.com/projectselector2/home/ "https://console.cloud.google.com/projectselector2/home/") y aquí dar seleccionamos "Crear proyecto". Seleccionamos el nombre que deseamos y añadimos una organización si tenemos una, en mi caso la dejaré en blanco.

### Añadiendo una API ### 
Luego de crear el proyecto se nos abrirá una ventana con los ajustes de nuestro proyecto, a la izquierda tiene un desplegable que muestra las opciones del menú, del desplegable seleccionaremos API y servicios.

Le daremos a la opción de "credenciales" y aquí pulsaremos la opción de arriba, con el texto "Crear credenciales" y crearemos una nueva "Clave de API". Esta clave podremos incorporarla en nuestro proyecto para que nuestra aplicación tipo maps responda de forma satisfactoria

### Restringiendo nuestra API para evitar uso indevido ###
Una vez nuestra API está creada, le damos al lapiz para editar los ajustes de nuestra API key, se nos abrirá un submenú con varias opciones, la que nos interesa es "Restricciones de aplicaciones, aquí seleccionaremos "Apps para android". Guardamos y ya está hecho.

## Preparando la Api Key desde nuestra aplicación android ##

### Añadiendo la key al local.properties ###
En nuestro local.properties debemos de añadir la linea "MAPS_API_KEY=APIKEY". Poniendo luego del "=" la Api Key generada hace unos cuantos apartados.

### Cambios en el build.gradle nivel proyecto ###
En nuestro build.gradle a nivel proyecto, tenemos que añadir esta estructura cómo la primera linea.
```java

buildscript {
    dependencies {
        classpath "com.google.android.libraries.mapsplatform.secrets-gradle-plugin:secrets-gradle-plugin:2.0.0"
    }
}
```
Esto permitirá utilizar la key si la colocamos en nuestro local.properties, lo que la hace más segura que tenerla simplemente en la lista de Strings.

### Cambios en el build.gradle nivel módulo ###
Dentro de los plugins debemos de añadir esta linea (En mi caso ya aparecía de forma predeterminada, creando una App de mapas)
```java

id 'com.google.android.libraries.mapsplatform.secrets-gradle-plugin'

```

### Cambios en el Manifest ###
Dentro de nuestro manifest, notaremos la etiqueta <meta-data>. nos interesa cambiar su android:value al valor que añadimos previamente en nuestro "local.properties". Podemos modificar esto cambiando el "android:value" tal y cómo en esta linea.

```java

android:value="${MAPS_API_KEY}" />
```

## Proyecto Android studio ##

### Añadiendo menú ###

En nuestra carpeta de resources damos click derecho y seleccionamos "Android Resources", donde añadimos uno de tipo menu. Aquí creamos un nuevo "menu resource file" y creamos uno llamado "menu.xml". El código que he utilizado es este.

```java

<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:android="http://schemas.android.com/apk/res/android">

    <item
        android:id="@+id/Area"
        android:title="Area en metros cuadrados" />
    <item
        android:id="@+id/Perimetro"
        android:title="Perimetro en metros" />
    <item
        android:id="@+id/Borrar"
        android:title="Borrar" />
</menu>
```

Se compone de varios items con su propio id y titulo, que serán aquellos que al ser seleccionados permitirán realizar una opción dentro de nuestro mapa.

### Añadiendo dependencias a nuestro gradle ###
Sólo es necesaria una dependencia adicional, esta es la que añadirá las funciones Spherical.util. Que nos facilitaran el calcular el area y el perimetro de nuestro poligono

```java

implementation 'com.google.maps.android:android-maps-utils:0.6.2'
```

### MapsActivity ###
Aquí es donde está la gran mayoría del código. Explicaré paso a paso lo más importante y el cómo lo he hecho.

Comenzamos haciendo que el mapa empiece en málaga y tenga una cinemática de inicio, para ello usaremos dos variables double, una con la latitud y otra con la longitud, se lo pasamos a un objeto tipo LatLng y acto seguido usamos Map.animateCamera() pasandole los datos adecuados para tener el punto de inicio que deseamos y con el zoom adecuado.

```java

 // Añadimos las coordenadas de Málaga y la animamos para que acceda directamente a la localización adecuada
        double latitud = 36.71961083514688;
        double longitud =  -4.453055248124418;
        LatLng malaga = new LatLng(latitud, longitud);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(malaga, 15), 7000, null);
```

En el onMapLongClick tomamos los valores de latitud y longitud del lugar donde hemos mantenido. En primer lugar me gustaría destacar que el condicional detecta si ya hay un punto inicial, si lo hay no hace nada, si no lo hay lo añade. Además de añadirlo muestra un mensaje con las coordenadas aclarando si fueran añadidos, y crea el icono adecuado.
```java
            public void onMapLongClick(LatLng latLng) {

                if(puntoInicialListo == false){
                    latLngList.add(latLng);

                    if(polyline != null) {
                        polyline.remove();
                    }

                    PolylineOptions polylineOptions = new PolylineOptions().addAll(latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);

                    mostrarMensaje("Coordenadas: " + latLng + "Añadidas correctamente");
                    marcador = mMap.addMarker(new MarkerOptions().position(latLng).title("Origen"));

                    puntoInicialListo = true;
                }
```
![alt text](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgZtjEJxPIBnbP1iDdpZcujbJr9_e-SBL7NhBvcHs_hCbHVHOrSe_Fy0kxg_sH2EKe0QxnUfC_m5udp_10g5iCWde5Y6y_Ahdhqyw_9sPIyBgTfq34AiQpdeDlZ5nSrjkGzXxdVMJ2YOR5N-c28cLT_Ve3-Yd_CNrSTH-l4xG8DEsDF7NBvDMIsKmXpSA/s520/Anadiendo_punto_inicial.PNG)


El método onMapClick verifica que haya un puntoInicial (El que creamos manteniendo). Si no lo hay no se realiza acción alguna. Si lo hay añadimos un nuevo vértice del polígono en la posición seleccionada

```java

public void onMapClick(LatLng latLng) {

                if(puntoInicialListo == true && cerrado == false){
                    latLngList.add(latLng);
                    if(polyline != null) {
                        polyline.remove();
                    }

                    PolylineOptions polylineOptions = new PolylineOptions().addAll(latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                } else {

                }

            }
        });
```
![Screenshot](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhtqnpLUFr6H-5YCwhaHoDnIj-3rNDmKPFPJKvP6MmiIbiaezmm_5rhCyrCrekh1p1k56uWH-0jgHM2rXlY9SvSuGmM5ahcPVAfSMoyHGj1fi6zXqVudOYaSqDJebxHpW2Wf2KH5x726nqD0bjIoEkZhH84aZWXOOZCYMdomEzjMjzDqlyVeV5Gl4OQ1g/s536/Anadiendo_varias_coordenadas.PNG)

Para añadir el menú que creamos anteriormente debemos de usar el  onCreateOptionsMenu() donde creamos un inflater e inflamos el menú.

```java
@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
```
La parte del código abajo, se encarga de aplicar los onClick del menú que inflamos a los métodos que mencionaré a continuación. Para calcular el area, calcular el perímetro y borrar las coordenadas.
```java
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Area:
                computeArea(latLngList);
                break;

            case R.id.Perimetro:
                computeLength(latLngList);
                break;

            case R.id.Borrar:
                metodoBorrar();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
```
![image](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgel2M47EzUvWllB2NGW1w2eQWwUHdJfpkhgWkpmY6N31Zvkkj5pOvY83TndHmnmFtKY3hjEGSIDmZcvKuor8wMA_4ygeLs1P3l6JT7tuHYObQ0BcRj-gV4pPLGsYvQg0HCQYcGqBWiAbhUr2F73ql1GxZIjDNAsdAO_A0mWLTlBuCE-kEX1aG-R-7Wjg/s515/Menu.PNG)

Nuestro método computeArea  es donde calculamos el area de nuestro polígono. Este requiere al menos 3 puntos (Contando cómo uno de ellos el propio marcador) ya que con 1 o 2 no podemos calcular un area. Cierra automáticamente nuestro polígono, calcula el área de la zona usando el método de Spherical.Util.computeArea().

```java
 public void computeArea(List<LatLng> latLngList){

        if(latLngList.size() >= 3){

            //Si no está cerrado se cierra
            if(!cerrado){
                this.latLngList.add(this.latLngList.get(0));
                if(polyline!=null){
                    polyline.remove();

                    //Si falla sacar del condicional estas lineas
                    PolylineOptions polylineOptions = new PolylineOptions().addAll(this.latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                    cerrado = true;
                }
            }

            area = SphericalUtil.computeArea(latLngList);
            areaAString = f.format(area);
            mostrarMensaje("Area: " + areaAString + " metros cuadrados");
        } else {

            mostrarMensaje("Hacen falta al menos tres puntos de coordenadas para calcular el area");

        }
```
![image](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgBwvfj4esDocal7Y_P3A_dE3xCVCMwpZ9aj6o6hiNVuAmxb7txUhtJqYzk3mHoY05ziI-lCHm0IFp8wB6Nb_aaw-5Ba9MYRu4_BBEhJIhK0MjrmTSufQKGGYhpvi9a0HgZe4KvMfldzfEUVpo1dqE_DF0In8rSkeybOJrujlJfOxPQq-_d9Xrz87ukBw/s509/Area_Metros_CuadradosPNG.PNG)

Este método de computeLength es similar al anterior computeArea, la única diferencia es que el método utilizado es SphericalUtil.computeLength()

```java
 public void computeLength(List<LatLng> latLngList){

        if(latLngList.size() >= 3){
            //Cerramos si no lo está ya
            if(!cerrado) {
                this.latLngList.add(this.latLngList.get(0));
                if (polyline != null) {
                    polyline.remove();

                    //Si falla sacar del condicional estas lineas
                    PolylineOptions polylineOptions = new PolylineOptions().addAll(this.latLngList).clickable(true);
                    polyline = mMap.addPolyline(polylineOptions);
                }
            }

            perimetro = SphericalUtil.computeLength(latLngList);
            perimetroAString = f.format(perimetro);
            mostrarMensaje("Perimetro: " +perimetroAString + " Metros");
            cerrado = true;

        } else {
            mostrarMensaje("Hacen falta al menos dos puntos de coordenadas para calcular su perimetro");
        }
```


Este otro método borra el marcador y todas las cooredenadas de la lista de coordenadas.

![image](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhw64s4Awbmh3s2NpERNMises18VBbbQgP2qzif1SEERdn9BnBbpHYPbpMyF8MilgE2fULoIz3oMy_9kxtCyqlJ1mP7LYqcuxGrSEr6McrepyXKpgwsznx_4aoWVB-YKZMbYmxJZt90K0g4L9uIlVe16Mg6oY-w6xOGKw_ZIPg2KVbUoQdD3z_F1eXrYg/s516/Perimetros_Metros_Cuadrados.PNG)

```java
 public void metodoBorrar(){
        if(polyline != null && latLngList.size() >= 1 ){
            polyline.remove();
            latLngList.clear();
            marcador.remove();
            puntoInicialListo = false;
            cerrado = false;
        } else {
            mostrarMensaje("Hace falta al menos una coordenada para poder borrar");
        }
    }
```
Y este otro sirve para escribir un toast con el mensaje que le pasemos
```java
    public void mostrarMensaje(String texto){
        Toast.makeText(MapsActivity.this, texto ,Toast.LENGTH_SHORT).show();
    }
```

## Incorporar imagenes en readme de github ##
Para incorporar imagenes, podemos usar por ejemplo las imagenes de nuestro servidor, sin embargo, en mi caso he optado por utilizar blogger con este fin.

Blogger es una plataforma de la propia google que nos permite crear blogs que siempre estarán presentes. Yo he creado un blog (Accesible desde este enlace: [https://imagenesraulvillodreshlcto05.blogspot.com/2022/04/blog-post.html](https://imagenesraulvillodreshlcto05.blogspot.com/2022/04/blog-post.html)).

En este blog hay varias imagenes, al darles click derecho y "abrir en otra pestaña" podemos ver el enlace de esta imagen.

Sin embargo, ¿por que hacerlo así en vez de en mi propio servidor? la respuesta es muy simple, mientras que nuestro servidor puede ser atacado por hackers o sufrir corrupción de datos por un virus o instalación mal realizada. Los bloggers de Google estarán ahí siempre y cuando la propia google exista. Así que estos datos estarán ahí siempre que google exista.
